%**************************************************************************
%*********  CONTROL DE TRAYECTORIA DE UN ROBOT OMNIDIRECCIONAL  ***********
%**************************************************************************

%% DATOS DE SIMULACION
 clear all; 
 close all; 
 clc;
 warning off;
 To = 0.01; 
 tf = 100;
 t = 0:To:tf;
%% Cargamos las librerias de la memoria compartida 
%Importar la memoria compartida
  loadlibrary('smClient64.dll','./smClient.h')
%Creamos la memoria compartida(En este caso lo isimos desde el panel)
%Abrir la memoria compartida 
  calllib('smClient64','openMemory','Velocidades',2)
  calllib('smClient64','openMemory','Posiciones',2)
  calllib('smClient64','openMemory','Seleccion',2)
%% Ponemos en cero todos los valores almacenados en la memoria
for i=0:2
    calllib('smClient64','setFloat','Velocidades',i,0);
    calllib('smClient64','setFloat','Posiciones',i,0);
    calllib('smClient64','setFloat','Seleccion',i,0);
end
%% CONSTANTES INICIALES 
  %Posici�n
   x(1)  = 0;
   y(1)  = 0;
   psi(1)= 0;
  %Velocidades
   uf(1)  = 0;
   ul(1)  = 0;
  %Desplazamiento
   a = 0; %Punto de interes desplazado
%% INICIO DE PROGRAMA AL MISMO TIEMPO UNITY Y MATLAB
 inicio = calllib('smClient64','getInt','Seleccion',0); 
 while ( inicio ~= 3 )%Condicion para iniciar al mismo tiempo unity y matlab
    inicio = calllib('smClient64','getInt','Seleccion',0);  %Se envia la condicion para que inicie el programa
 end
 trayectoria = calllib('smClient64','getInt','Seleccion',1);
%% TRAYECTORIAS DESEADAS (SET-POINT)
    if (trayectoria == 1)
        %Trayectoria de un Seno en 'X'
            hxd = 0.1*t;                         
            hyd = sin(.2*t);  
            hxdp = 0.1*ones(1,length(t)); 
            hydp = .2*cos(.2*t);  
    elseif (trayectoria == 2)
        % Trayectoria de un corazon
            hxd1 = -(12*sin(0.1*t)-4*sin(3*0.1*t))/4; 
            hyd1= (13*cos(0.1*t)-5*cos(2*0.1*t)-2*cos(3*0.1*t)-cos(4*0.1*t))/4;
            hxd=hxd1+0;
            hyd=hyd1+1;
            hxdp= diff([0 hxd]); 
            hydp= diff([0 hyd]);
    elseif (trayectoria == 3)
        % Trayectoria de un ocho o infinito
            hxd = 1.5*cos(0.2*t);                          
            hyd = 1.5*sin(0.4*t); 
            hxdp = -1.5*0.2*sin(0.2*t);
            hydp = 1.5*0.4*cos(0.4*t);
    elseif (trayectoria == 4)
        %Trayectoria de un circulo
            hxd = 4*cos(0.2*t); 
            hyd= 4*sin(0.2*t); 
            hxdp= -4*0.2*sin(0.2*t); 
            hydp= 4*0.2*cos(0.2*t);     
    end
%% Cinem�tica Directa 
   hx = x + a*cos(psi);
   hy = y + a*sin(psi);  
%% **************************** CONTROLADOR *******************************   
for k=1:length(t)
%% a) Errores de control
    hxe(k)  = hxd(k) - hx(k); 
    hye(k)  = hyd(k) - hy(k);
    he      = [hxe(k) hye(k)]';    
%% b) Jacobiano
    j11 = cos(psi(k));
    j12 = -sin(psi(k));
    
    j21 = sin(psi(k));
    j22 = cos(psi(k));
        
    J = [j11 j12;j21 j22];
%% c) Matriz de Peso
    W = diag([5,5]);    
%% d) Ley de Control
    vd = [hxdp(k) hydp(k)]';
    vref = inv(J)*(vd + W*tanh(1*he));
    ul(k) = vref(1);
    uf(k) = vref(2);
%% e) Enviamos Las Velocidades y Angulo a UNITY 
    tic
    calllib('smClient64','setFloat','Velocidades',0,ul(k));
    calllib('smClient64','setFloat','Velocidades',1,uf(k));
    calllib('smClient64','setFloat','Velocidades',2,psi(k));
    
    hx(k+1) = calllib('smClient64','getFloat','Posiciones',0);
    hy(k+1) = calllib('smClient64','getFloat','Posiciones',1);
    psi(k+1) = calllib('smClient64','getFloat','Posiciones',2);
    
%% f) Tiempo de muestro
     while (toc < To)
     end
     dt(k)= toc;
end



%************************************************************************** 
%***************************ANIMACI�N****************************************
%% ************************************************************************** 
%% Simulacion

pasos=10;  fig=figure('Name','Simulacion');

set(fig,'position',[60 60 980 600]);
axis square; cameratoolbar
axis([-4.5 4.5 -4.5 4.5 0 1]);
grid on
Dimension_Omni(3);
M1=Plot_Omni(hx(1),hy(1),psi(1));hold on
M2=plot(hx(1),hy(1),'b','LineWidth',1.5);
plot(hxd,hyd,'r','LineWidth',1.5);

for i=1:pasos:length(t)
    
    delete (M1)
    delete (M2)
    M1=Plot_Omni(x(i),y(i),psi(i)); hold on
    M2=plot(hx(1:i),hy(1:i),'b','LineWidth',1.5);
    
    pause(To)
    
end

%************************************************************************** 
%***************************GR�FICAS****************************************
%% ************************************************************************** 
figure
subplot(2,1,1);
    plot(uf,'b','LineWidth',1.5),hold on, 
    title('Velocidad lineal frontal (uf)')
    xlabel('Tiempo [s]')
    ylabel('[m]')
    legend('uf Maniobrabilidad')
    grid on

subplot(2,1,2);
    plot(ul,'b','LineWidth',1.5), hold on,
    title('Velocidad lineal lateral (ul)')
    xlabel('Tiempo [s]')
    ylabel('[m]')
    legend('ul Maniobrabilidad')
    grid on

figure
    plot(t,hxe,'r','LineWidth',1), hold on
    plot(t,hye,'b','LineWidth',1), hold on
    legend('Error x', 'Error y')
    xlabel('Tiempo [s]')
    ylabel('[m]')
    title('Errores de Control')
    grid on

