﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Runtime.InteropServices;


public class Trayectoria1 : MonoBehaviour
{
    //********************************************* LIBRERIAS PARA MEMORIA COMPARTIDA *************************************************
    const string dllPath = "smClient64.dll";
    //declarar las funciones de la dll
    [DllImport(dllPath)] // For 64 Bits System
    static extern int openMemory(String name, int type);

    [DllImport(dllPath)] // For 64 Bits System
    static extern void setInt(String memName, int position, int value);
    //*********************************************************************************************************************************

    //************************************ VARIABLE PARA TRABAJAR CON MEMORIA COMPARTIDA **********************************************
    private string memoriaSeleccion = "Seleccion";
    //*********************************************************************************************************************************
    void Start()
    {
        openMemory(memoriaSeleccion, 2);
    }
    void Update(){}
     //Creamos funciones de tipo publica para asignar las diferentes trayectorias (SET-POINT) definidas en unity
        //Trayectoria senoidal
        public void trayectoria1()
        {
            setInt(memoriaSeleccion, 1, 2); 
        }

        //Trayectoria corazon
        public void trayectoria2()
        {
            setInt(memoriaSeleccion, 1, 1);
        }

        //Trayectoria infinito
        public void trayectoria3()
        {
            setInt(memoriaSeleccion, 1, 3);
        }

        //Trayectoria circulo
        public void trayectoria4()
        {
            setInt(memoriaSeleccion, 1, 4);
        }
}