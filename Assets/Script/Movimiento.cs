﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using System.Runtime.InteropServices;

public class Movimiento : MonoBehaviour
{
    //************************************* LIBRERIAS PARA MEMORIA COMPARTIDA *********************************************
    const string dllPath = "smClient64.dll";

    [DllImport(dllPath)] // For 64 Bits System
    static extern int openMemory(String name, int type);

    [DllImport(dllPath)]
    static extern float getFloat(String memName, int position);

    [DllImport(dllPath)] // For 64 Bits System
    static extern void setInt(String memName, int position, int value);

    [DllImport(dllPath)]
    static extern void setFloat(String memName, int position, float value);
    //***********************************************************************************************************************

    //************************************ VARIABLE PARA TRABAJAR CON MEMORIA COMPARTIDA ************************************
    private string memoriaVelocidades = "Velocidades";
    private string memoriaPosiciones = "Posiciones";
    private string memoriaSeleccion = "Seleccion";
    //***********************************************************************************************************************

    //*********************************************** VARIABLES PARA EL PROCESO *********************************************
    public Vector3 posInicial = new Vector3(0, 0, 0);
    public float vel = 0f;
    private float To;         // Tiempo de muestreo
    private float TimeSample; // Referencia del tiempo de muestreo
    // Start is called before the first frame update
    private float time;
    private float ul;
    private float uf;
    private float psi;
    private double hx;
    private double hy;
    private float hx1;
    private float hy1;
    private double hxp;        // Proceso - Diferencial de altura del agua en el tanque
    private double hyp;          // Altura de agua en el tanque
    //***********************************************************************************************************************

    void Start()
    {
        transform.position = posInicial;
        openMemory(memoriaVelocidades, 2);
        openMemory(memoriaPosiciones, 2);
        openMemory(memoriaSeleccion, 2);
        for (int i = 0; i < 3; i++)
        {
            Debug.Log(i);
            setFloat(memoriaVelocidades, i, 0);
            setFloat(memoriaPosiciones, i, 0);
        }
        To = 0;   // [s]
        TimeSample = 0.01f; //[s]
        setInt(memoriaSeleccion, 0, 3);
    }

    // Update is called once per frame
    void Update()
    {
        ModeloCinematico();
    }

    private void ModeloCinematico()
    {
        To += Time.deltaTime; // Time.deltaTime = 1/fps, si 60 fps -> Time.deltaTime = 0.01666 [s] = 16.66 [ms] 

        if (To >= TimeSample)
        {
            //***************************************** RECIBE DATOS DE MATLAB ***********************************************
            ul = getFloat(memoriaVelocidades, 0);
            uf = getFloat(memoriaVelocidades, 1);
            psi = getFloat(memoriaVelocidades, 2);
            //********************************** MODELO CINEMATICO ROBOT OMNIDIRECCIONAL *************************************
               //Aplicando velocidades del controlador
                hxp = (ul * Math.Cos(psi)) - (uf * Math.Sin(psi));
                hyp = (ul * Math.Sin(psi)) + (uf * Math.Cos(psi));
               // Pociones del robot (Integracion numerica Metodo de Euler) ---------------> Datos que se envian a escena
                hx = To * hxp + hx;
                hy = To * hyp + hy;
               // Convertimos en datos doubles en flotantes.
                hx1 = Convert.ToSingle(hx);
                hy1 = Convert.ToSingle(hy);
               //Cambiamos las posiciones en la escena
                Vector3 temp = transform.position;
                    temp.x = hx1 + vel;
                    temp.z = hy1 + vel;
                transform.position = temp;
            //******************************************** ENVIA DATOS A MATLAB **********************************************
            setFloat(memoriaPosiciones, 0, hx1);
            setFloat(memoriaPosiciones, 1, hy1);
            setFloat(memoriaPosiciones, 2, psi);
            To = 0;
        }
    }
}