﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SeleccionCanvas : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CargaEscena(string Escena)
    {
        SceneManager.LoadScene(Escena);//Cargamos la siguiente escena en la que se encuentra el robot
    }
}
